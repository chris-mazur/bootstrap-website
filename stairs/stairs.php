<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Positive Project 2000 LTD - Main Building Contractors">
<meta name="keywords" content="main contractor, extension, conversion, conservation, renovation, brickwork, ground work, refurbishment, roof, electrical, plumbing, terrace, decoration, landscaping, prefabricated stairs, prefabricated concrete stairs, hampstead, london, camden, camden town">
<meta name="author" content="Mariusz Wasowski, Krzysztof Mazur">
<link rel="stylesheet" type="text/css" href="style_stairs.css">
<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />
<title>Stairs</title>
<script src="./js/gallery.js"></script>
<script src="js/jquery-1.11.0.min.js"></script>
<script src="js/lightbox.min.js"></script>
<link href="lightbox.css" rel="stylesheet" />
<script src="js/gallery.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54576502-1', 'auto');
  ga('send', 'pageview');

</script>

</head>

<body>
<div id="container">
	<div id="logo">
    <img src="../img/positive-project_logo_large.jpg" alt="logo">
	</div>
    <div id="menu">
    <?php 
	include ('./sections/menu.php');
	?>
	</div>

    <div id="main">
   <div id="welcome">Stairs</div>
   <div id="main_text">
   <p class="stair_title">How are our stairs built?</p>
    <p style="text-align:justify;">Prefabricated units are molded steps made of cement mixed with sand of appropriate grain size. Prefabricated stairs offer the possibility to be combined freely, which permits to create eye-catching spiral stairs structures.</p>
    <p class="stair_title">Unlimited finishing</p>
    <p style="text-align:justify;">
    Using prefabricated units permits to freely select finishing materials. The concrete surface can be covered with plaster, stucco or terrazzo. The steps may be made of granite, stoneware, wood. Risers can be made of granite, stoneware, wood, acid-proof metal sheet. Handrails and balustrades are made of acid- proof steel or brass – with longitudinal or vertical fill.
Well designed and constructed spiral stairs not only guarantee safety, but ensure comfort to their users as well. Such stairs can be perfectly integrated into modern interiors. The construction of prefabricated stairs is based on measurements made taking into account full functionality and expectations of each client.
    </p>
    <p class="stair_title">Our offer</p>
    <p style="text-align:justify;">
    The stairs diameter ranges from 165 to 300cm. Varied diameters and shapes of prefabricated stairs offer the possibility to use them not only in private interiors, but also in public buildings. Such stairs may also be used as external fire escape. 
    </p>
    <p class="stair_title">Contact Us if want to know more</p>
    <p style="text-align:center;">Email us: <a href="mailto:positiveproject2000@hotmail.com" title="Enquiry from website" style="text-align:center; font-weight:bold; color:#37b751;">positiveproject2000@hotmail.com</a></p>
     <p style="text-align:center;">
    Call us: <a href="tel:+447859803608" style="text-align:center; font-weight:bold; color:#37b751;">+44 7859 803608</a> or 
    <a href="tel:+447500748253" style="text-align:center; font-weight:bold; color:#37b751;">+44 7500 748253</a>
    </p>
    <p style="text-align:center;">
    <a href="./gallery_1/1.png" target="photo" data-lightbox="stairs1"><img src="gallery_1/main_miniature.png"></a>
    <a href="./gallery_2/1.png" target="photo" data-lightbox="stairs2"><img src="gallery_2/main_miniature.png"></a>
    <a href="./gallery_3/1.png" target="photo" data-lightbox="stairs3"><img src="gallery_3/main_miniature.png"></a><br>
    <a href="./gallery_4/1.png" target="photo" data-lightbox="stairs4"><img src="gallery_4/main_miniature.png"></a>
    <a href="./gallery_5/1.png" target="photo" data-lightbox="stairs5"><img src="gallery_5/main_miniature.png"></a>
    <a href="./gallery_6/1.png" target="photo" data-lightbox="stairs6"><img src="gallery_6/main_miniature.png"></a><br>
    
<a href="./gallery_1/2.png" target="photo" data-lightbox="stairs1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_1/3.png" target="photo" data-lightbox="stairs1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_1/4.png" target="photo" data-lightbox="stairs1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_1/5.png" target="photo" data-lightbox="stairs1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_1/6.png" target="photo" data-lightbox="stairs1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_1/7.png" target="photo" data-lightbox="stairs1" style="visibility:hidden; font-size:1px;">img</a>

<a href="./gallery_2/2.png" target="photo" data-lightbox="stairs2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_2/3.png" target="photo" data-lightbox="stairs2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_2/4.png" target="photo" data-lightbox="stairs2" style="visibility:hidden; font-size:1px;">img</a>

<a href="./gallery_3/2.png" target="photo" data-lightbox="stairs3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_3/3.png" target="photo" data-lightbox="stairs3" style="visibility:hidden; font-size:1px;">img</a>

<a href="./gallery_4/2.png" target="photo" data-lightbox="stairs4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_4/3.png" target="photo" data-lightbox="stairs4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./gallery_4/4.png" target="photo" data-lightbox="stairs4" style="visibility:hidden; font-size:1px;">img</a>

<a href="./gallery_5/2.png" target="photo" data-lightbox="stairs5" style="visibility:hidden; font-size:1px;">img</a>

<a href="./gallery_6/2.png" target="photo" data-lightbox="stairs6" style="visibility:hidden; font-size:1px;">img</a>
</p>
	</div>
    </div>
    <div id="footer">
    <div id="footer1">
    Copyright © Positive Project 2000 Ltd 2014
    </div>
    <div id="footer2">
    Corporate Info | Terms & Conditions | Privacy Policy
    </div>
  </div>
</div>
<a href="https://plus.google.com/116000541812327228781" rel="publisher" style="visibility:hidden;">Google+</a>
</body>
</html>
