<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Positive Project 2000 LTD - Main Building Contractors">
<meta name="keywords" content="main contractor, extension, conversion, conservation, renovation, brickwork, ground work, refurbishment, roof, electrical, plumbing, terrace, decoration, landscaping, prefabricated stairs, prefabricated concrete stairs, hampstead, london, camden, camden town">
<meta name="author" content="Mariusz Wasowski, Krzysztof Mazur">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />
<title>Contact</title>
<script src="js/gallery.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54576502-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<div id="container">
	<div id="logo">
    <img src="img/positive-project_logo_large.jpg">
	</div>
    <?php 
	include ('./sections/announcement.php');
	?>
    <div id="menu">
    <?php 
	include ('./sections/menu.php');
	?>
	</div>
    <div id="gallery">
      
      <?php
	include'./slideshow/slideshow.php';
	?>
      
    </div>
    <div id="main">
   <div id="welcome">Contact</div>
   <div id="main_text">
   <p style="text-align:center; padding:20px;"><img src="img/positive-project_logo_small.jpg"><br>
    42 Merton Avenue<br>
    Northolt, London, UB5 4QF
    </p>
    <p style="text-align:center;">Email us: <a href="mailto:positiveproject2000@hotmail.com" title="Enquiry from website" style="text-align:center; font-weight:bold; color:#37b751;">positiveproject2000@hotmail.com</a></p>
    <p style="text-align:center;">
    Call us: <a href="tel:+447859803608" style="text-align:center; font-weight:bold; color:#37b751;">+44 7859 803608</a> or 
    <a href="tel:+447437416680" style="text-align:center; font-weight:bold; color:#37b751;">+44 7437 416680</a>
    </p>
	</div>
    </div>
    <div id="footer">
    <div id="footer1">
    Copyright © Positive Project 2000 Ltd 2014
    </div>
    <div id="footer2">
    Corporate Info | Terms & Conditions | Privacy Policy
    </div>
  </div>
</div>
</body>
</html>
