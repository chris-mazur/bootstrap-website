<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Positive Project 2000 LTD - Main Building Contractors">
<meta name="keywords" content="main contractor, extension, conversion, conservation, renovation, brickwork, ground work, refurbishment, roof, electrical, plumbing, terrace, decoration, landscaping">
<meta name="author" content="Mariusz Wasowski, Krzysztof Mazur">
<script src="js/jquery-1.11.0.min.js"></script>
<script src="js/lightbox.min.js"></script>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />
<link href="lightbox.css" rel="stylesheet" />
<title>Projects</title>
<script src="js/gallery.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54576502-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<div id="container">
	<div id="logo">
    <img src="img/positive-project_logo_large.jpg">
	</div>
    <?php 
	include ('./sections/announcement.php');
	?>
    <div id="menu">
    <?php 
	include ('./sections/menu.php');
	?>
	</div>
    <div id="gallery">
      
      <?php
	include'./slideshow/slideshow.php';
	?>
      
    </div>
    <div id="main">
   <div id="welcome">Projects</div>
   <div id="main_text">
   <?php
	include'./projects/hampstead1.php';
	?>
    
     <?php
	include'./projects/southhillpark.php';
	?>
 
    <?php
	include'./projects/hampstead2.php';
	?>
    <?php
	include'./projects/hampstead3.php';
	?>
    <?php
	include'./projects/hampstead4.php';
	?>
    <?php
	include'./projects/stjohnswood1.php';
	?>
    <?php
	include'./projects/hollandpark1.php';
	?>
    <?php
	include'./projects/harleystreet1.php';
	?>
    <?php
	include'./projects/hendon1.php';
	?>
    <?php
	include'./projects/islington1.php';
	?>
	</div>
    </div>
    <div id="footer">
    <div id="footer1">
    Copyright © Positive Project 2000 Ltd 2014
    </div>
    <div id="footer2">
    Corporate Info | Terms & Conditions | Privacy Policy
    </div>
  </div>
</div>
</body>
</html>


