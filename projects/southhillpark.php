﻿<div class="project_container">
<div class="miniature">
<img src="./projects/southhillpark/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
South Hill Park Gardens
</p>
<p style="text-align: justify;">
Use of property as a single family dwellinghouse. Erection of part single and part three-storey rear extensions, erection of rear dormer window and installation of three rooflights (2 x front roofslope and 1 x rear roofslope) in connection with single family dwellinghouse.
</p>
<div class="button"><a href="./projects/southhillpark/1.png" target="photo" data-lightbox="southhillpark">View all</a>
<a href="./projects/southhillpark/2.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/3.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/4.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/5.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/6.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/7.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/8.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/9.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/10.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/11.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/12.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/13.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/14.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/15.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/16.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/17.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/18.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/19.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/southhillpark/20.png" target="photo" data-lightbox="southhillpark" style="visibility:hidden; font-size:1px;">img</a>




</div>
</div>
<div class="separating_line"></div>
</div>