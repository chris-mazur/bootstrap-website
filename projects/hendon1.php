﻿<div class="project_container">
<div class="miniature">
<img src="./projects/hendon1/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Hendon
</p>
<p style="text-align: justify;">
Our clients were successful in their application for planning permission for front, side and rear extensions and a garage conversion. The full scope of their project went way beyond that to include a complete overhaul of the existing property.
</p>
<p style="text-align: justify;">
Features of the scheme included a sauna and separate steam room, the installation of a Succah roof light, staircase light well and stonework. The property was redecorated throughout and external works included the complete overhaul of the existing roof and soft landscaping.
</p>
<div class="button"><a href="./projects/hendon1/1.png" target="photo" data-lightbox="hendon1">View all</a>
<a href="./projects/hendon1/2.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/3.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/4.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/5.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/6.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/7.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/8.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/9.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/10.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/11.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/12.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/13.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/14.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hendon1/15.png" target="photo" data-lightbox="hendon1" style="visibility:hidden; font-size:1px;">img</a>



</div>
</div>
<div class="separating_line"></div>
</div>