﻿<div class="project_container">
<div class="miniature">
<img src="./projects/stjohnswood1/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
St. John's Wood
</p>
<p style="text-align: justify;">
A striking feature of this project in St. John's Wood is the abundance of natural light sources incorporated within the design. The development included a ground floor extension to the rear and the complete refurbishment of the property on contemporary modern lines.
</p>
<p style="text-align: justify;">
Our clients wrote of us, ''...we were very impressed with the work and found them reliable, honest and hardworking. We would highly recommend them''.
</p>
<div class="button"><a href="./projects/stjohnswood1/1.png" target="photo" data-lightbox="stjohnswood1">View all</a>
<a href="./projects/stjohnswood1/2.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/3.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/4.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/5.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/6.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/7.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/8.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/9.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/10.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/stjohnswood1/11.png" target="photo" data-lightbox="stjohnswood1" style="visibility:hidden; font-size:1px;">img</a>




</div>
</div>
<div class="separating_line"></div>
</div>