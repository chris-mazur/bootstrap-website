﻿<div class="project_container">
<div class="miniature">
<img src="./projects/hampstead1/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Hampstead
</p>
<p style="text-align: justify;">
The complete refurbishment of this Victorian house, located at the bottom of Parliament Hill, included the conversion of two flats into single dwelling and construction of a side extension with a feature brick wall and unified internal floor and garden levels.  The floor has been opened in the rear bedrooms to create a two storey library. The project also included new bathrooms, kitchen and an upgrade of the original staircase. New electrical, plumbing and heating services were installed throughout.
</p>
<div class="button"><a href="./projects/hampstead1/1.png" target="photo" data-lightbox="hampstead1">View all</a>
<a href="./projects/hampstead1/2.png" target="photo" data-lightbox="hampstead1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead1/3.png" target="photo" data-lightbox="hampstead1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead1/4.png" target="photo" data-lightbox="hampstead1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead1/5.png" target="photo" data-lightbox="hampstead1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead1/6.png" target="photo" data-lightbox="hampstead1" style="visibility:hidden; font-size:1px;">img</a>
</div>
</div>
<div class="separating_line"></div>
</div>