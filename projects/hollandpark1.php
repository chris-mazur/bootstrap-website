﻿<div class="project_container">
<div class="miniature">
<img src="./projects/hollandpark1/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Holland Park
</p>
<p style="text-align: justify;">
This Grade 2 listed building had fallen into a state of some disrepair. Our brief was to convert two existing apartments into one high spec dwelling. The project required the sympathetic overhaul of original features such as box sash windows and cornice. New hardwood timber floorboards were installed throughout, new plumbing, heating and electrical services provided and high standards of finish to the new kitchen, bathrooms and décor were delivered on time and on budget.
</p>
<p style="text-align: justify;">
'' I would be delighted to recommend Positive Project 2000 to anyone considering a house renovation. There were a number of challenging aspects relating to listed buildings restrictions and strict rules relating to noise, dust and disturbance to neighbours. The job was completed on time and within budget. The workers are professional, polite and they keep a very clean site.''
</p>
<div class="button"><a href="./projects/hollandpark1/1.png" target="photo" data-lightbox="hollandpark1">View all</a>
<a href="./projects/hollandpark1/2.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/3.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/4.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/5.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/6.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/7.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/8.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/9.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/10.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hollandpark1/11.png" target="photo" data-lightbox="hollandpark1" style="visibility:hidden; font-size:1px;">img</a>




</div>
</div>
<div class="separating_line"></div>
</div>