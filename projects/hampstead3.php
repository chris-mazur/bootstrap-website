﻿<div class="project_container">
<div class="miniature">
<img src="./projects/hampstead3/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Hampstead
</p>
<p style="text-align: justify;">
Extensive additions to this property in the heart of Hampstead included the removal of the original roof structure, the addition of an entirely new third storey, the creation of rooms within the new roof space and an extension to the ground floor.
</p>
<p style="text-align: justify;">
Our clients were delighted with the outcome, ''...Positive Project 2000 were highly professional and it was particularly impressive how each individual clearly took real pride in their work and thought things through with intelligence and obvious experience… We would have no hesitation in recommending Positive Project 2000 to anyone considering a similar major refurbishment''.
</p>
<div class="button"><a href="./projects/hampstead3/1.png" target="photo" data-lightbox="hampstead3">View all</a>
<a href="./projects/hampstead3/2.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/3.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/4.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/5.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/6.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/7.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/8.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/9.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead3/10.png" target="photo" data-lightbox="hampstead3" style="visibility:hidden; font-size:1px;">img</a>

</div>
</div>
<div class="separating_line"></div>
</div>