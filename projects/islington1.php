﻿<div class="project_container">
<div class="miniature">
<img src="./projects/islington1/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Islington
</p>
<p style="text-align: justify;">
Located in the conservation area of Barnsbury, Islington, the property required more than just a few sympathetic improvements. A new brick built conservatory added elegant living space, roof lights were added to provide more natural light sources within the home and the property was fully refurbished, including the repair and replacement of ornate cornice and the re-adoption of original vaults and basement.
</p>
<p style="text-align: justify;">
''Positive Project 2000 refurbished my delapidated listed Georgian property efficiently, on schedule and on budget. I would employ them again.''
</p>
<div class="button"><a href="./projects/islington1/1.png" target="photo" data-lightbox="islington1">View all</a>
<a href="./projects/islington1/2.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/3.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/4.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/5.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/6.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/7.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/8.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/9.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/10.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/islington1/11.png" target="photo" data-lightbox="islington1" style="visibility:hidden; font-size:1px;">img</a>




</div>
</div>
</div>