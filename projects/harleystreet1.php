﻿<div class="project_container">
<div class="miniature">
<img src="./projects/harleystreet1/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Harley Street
</p>
<p style="text-align: justify;">
The refurbishment of a Harley Street medical consulting house required the standard of finishes one would expect in such a prestigious location. Works included the extension of an existing lift shaft, structural alterations, new service installations and a variety of improvements carried out in a manner sympathetic to this period property.</p>
<p style="text-align: justify;">
Our client wrote, ''We set a challenging schedule and very much appreciated the flexibility demonstrated by Positive Project 2000 in accommodating changes that became necessary for the realisation of our project and without any adverse effect on the completion date. The project was competitively priced and finished on time''.
</p>
<div class="button"><a href="./projects/harleystreet1/1.png" target="photo" data-lightbox="harleystreet1">View all</a>
<a href="./projects/harleystreet1/2.png" target="photo" data-lightbox="harleystreet1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/harleystreet1/3.png" target="photo" data-lightbox="harleystreet1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/harleystreet1/4.png" target="photo" data-lightbox="harleystreet1" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/harleystreet1/5.png" target="photo" data-lightbox="harleystreet1" style="visibility:hidden; font-size:1px;">img</a>


</div>
</div>
<div class="separating_line"></div>
</div>