﻿<div class="project_container">
<div class="miniature">
<img src="./projects/hampstead2/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Hampstead
</p>
<p style="text-align: justify;">
Challenging ground conditions and restricted site access meant that very careful excavation by hand and the subsequent construction of retaining walls and roof slab was necessary to form this luxurious garden pool and spa room. Features included the provision of a modern stainless steel spiral staircase and a host of natural light sources. Our engineers installed Lutron Homeworks, video entry, AV and a telephone system.</p>
<p style="text-align: justify;">
The main property originally comprised two semi detached houses, which have been combined and refurbished. Extensive alterations and additions include the manufacture and installation of a variety of purpose made joinery and the overhaul and upgrading of the main staircase. Our engineers have installed a Unico heating & cooling system and the house has been completely rewired.
</p>
<div class="button"><a href="./projects/hampstead2/1.png" target="photo" data-lightbox="hampstead2">View all</a>
<a href="./projects/hampstead2/2.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/3.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/4.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/5.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/6.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/7.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/8.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/9.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/10.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/11.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/12.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/13.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/14.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/15.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/16.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/17.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/18.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/19.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/20.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/21.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead2/22.png" target="photo" data-lightbox="hampstead2" style="visibility:hidden; font-size:1px;">img</a>
</div>
</div>
<div class="separating_line"></div>
</div>