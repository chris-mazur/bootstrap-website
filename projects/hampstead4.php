﻿<div class="project_container">
<div class="miniature">
<img src="./projects/hampstead4/main_miniature.png">
</div>
<div class="project_description">
<p style="color:black; font-size:15px; font-weight:bold;">
Hampstead
</p>
<p style="text-align: justify;">
The excavation and construction of a new basement and formation of a predominantly glazed rear extension were the main additions to this Hampstead Heath residence. The refurbishment of the existing house included the manufacture in oak and installation of four new staircases, by our own team of joiners.</p>
<p style="text-align: justify;">
''Your attention to detail and hardworking team enabled us to realise our vision of what we wanted our house to be. You pulled all the elements together and created a luxurious house, melding period features with modern hi-tech wizardry in a seamless fashion. We cannot begin to thank you enough for the wonderful home you built for us.''
</p>
<div class="button"><a href="./projects/hampstead4/1.png" target="photo" data-lightbox="hampstead4">View all</a>
<a href="./projects/hampstead4/2.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/3.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/4.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/5.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/6.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/7.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/8.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/9.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/10.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/11.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/12.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/13.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/14.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>
<a href="./projects/hampstead4/15.png" target="photo" data-lightbox="hampstead4" style="visibility:hidden; font-size:1px;">img</a>

</div>
</div>
<div class="separating_line"></div>
</div>