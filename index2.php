<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Positive Project 2000 LTD - Main Building Contractors">
<meta name="keywords" content="main contractor, extension, conversion, conservation, renovation, brickwork, ground work, refurbishment, roof, electrical, plumbing, terrace, decoration, landscaping">
<meta name="author" content="Mariusz Wasowski, Krzysztof Mazur">
<link rel="stylesheet" type="text/css" href="style.css">
<title>Positive Project 2000 LTD</title>
<script src="js/gallery.js"></script>
</head>

<body>
<div id="container">
	<div id="logo">
    <img src="img/positive-project_logo_large.jpg">
	</div>
    <div id="menu">
    <?php 
	include ('./sections/menu.php');
	?>
	</div>
    <div id="gallery">
      
	  <?php
	include'./slideshow/slideshow.php';
	?>
      
    </div>
    <div id="main">
   <div id="welcome">Welcome</div>
   <div id="main_text">
    <p>Critical to the success of any building project is the appointment of the right builder for the job. At Positive Project 2000 we have a passion for quality, not 
just in our finished product but in every aspect of our work. Builders are notorious for delays and vague quotations. We are punctual, efficient and precise.</p>

<p>Despite poor economic conditions, we have experienced consistent growth in demand for our services over the past four years. This reflects the value we
place in nurturing relationships and turning customer expectations into reality. We deliver on the promises we make.</p>

<p>In turn our clients choose to use us again and they recommend us to others.</p>

<p>The best time to contact us is at an early stage in the planning of your project.</p>
	</div>
    </div>
    <div id="footer">
    <div id="footer1">
    Copyright © Positive Project 2000 Ltd 2014
    </div>
    <div id="footer2">
    Corporate Info | Terms & Conditions | Privacy Policy
    </div>
  </div>
</div>
</body>
</html>
