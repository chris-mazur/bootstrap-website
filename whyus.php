<!doctype html>
<html>
<head>
<title>Why Us?</title>
<meta charset="UTF-8">
<meta name="description" content="Positive Project 2000 LTD - Main Building Contractors">
<meta name="keywords" content="main contractor, extension, conversion, conservation, renovation, brickwork, ground work, refurbishment, roof, electrical, plumbing, terrace, decoration, landscaping, prefabricated stairs, prefabricated concrete stairs, hampstead, london, camden, camden town">
<meta name="author" content="Mariusz Wasowski, Krzysztof Mazur">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />
<title>Why Us?</title>
<script src="js/gallery.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54576502-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<div id="container">
	<div id="logo">
    <img src="img/positive-project_logo_large.jpg">
	</div>
    <?php 
	include ('./sections/announcement.php');
	?>
    <div id="menu">
    <?php 
	include ('./sections/menu.php');
	?>
	</div>
    <div id="gallery">
      
       <?php
	include'./slideshow/slideshow.php';
	?>
      
    </div>
    <div id="main">
   <div id="welcome">Why us?</div>
   <div id="main_text">
<p>Client testimonials suggest five main reasons for choosing Positive Project 2000.</p>
    <p>
    <li class="dotlist">accurate and comprehensive fixed price quotations</li>
<li class="dotlist">commitment to sticking to budget and completing on time</li>
<li class="dotlist">positive attitude, punctuality and reliability of staff and management</li>
<li class="dotlist">responsiveness when problems arise</li>
<li class="dotlist">quality of finished work</li>
    </p>
    <p>Do not hesitate to contact us to discuss how we may be able to help you.</p>
	</div>
    </div>
    <div id="footer">
    <div id="footer1">
    Copyright © Positive Project 2000 Ltd 2014
    </div>
    <div id="footer2">
    Corporate Info | Terms & Conditions | Privacy Policy
    </div>
  </div>
</div>
</body>
</html>


