<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Positive Project 2000 LTD - Main Building Contractors">
<meta name="keywords" content="main contractor, extension, conversion, conservation, renovation, brickwork, ground work, refurbishment, roof, electrical, plumbing, terrace, decoration, landscaping, prefabricated stairs, prefabricated concrete stairs, hampstead, london, camden, camden town">
<meta name="author" content="Mariusz Wasowski, Krzysztof Mazur">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />
<title>Our Clients</title>
<script src="js/gallery.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54576502-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<div id="container">
	<div id="logo">
    <img src="img/positive-project_logo_large.jpg">
	</div>
    <?php 
	include ('./sections/announcement.php');
	?>
    <div id="menu">
    <?php 
	include ('./sections/menu.php');
	?>
	</div>
    <div id="gallery">
      
       <?php
	include'./slideshow/slideshow.php';
	?>
      
    </div>
    <div id="main">
   <div id="clients">Our Clients</div>
   <div id="main_text">
  <p>Our clients tend to have discerning tastes and high levels of expectation. Following a recent refurbishment in Harley Street, we received this testimonial:</p>
    <p>"We appointed Positive Project 2000 for the refurbishment of a Georgian medical consulting house in Harley Street. The scope of the work was extensive.
    </p>
    <p style="text-align:justify;">We set a challenging schedule and were extremely pleased with the way in which Positive Project 2000 entered into the project with enthusiasm, professionalism and sympathy for the particular requirements of a period building. We very much appreciated their flexibility in accommodating changes that became necessary, without any adverse effect on the completion date.  The project was competitively priced and finished on time.</p>
    <p>We enjoy the relationship we have developed with Positive Project 2000 and look forward to working with them in the future."</p>
    <p>Copy of this and other letters of recommendation are available for your perusal and prospective clients are invited to visit some of our projects and to speak to existing and past clients.</p>
	</div>
    </div>
    <div id="footer">
    <div id="footer1">
    Copyright © Positive Project 2000 Ltd 2014
    </div>
    <div id="footer2">
    Corporate Info | Terms & Conditions | Privacy Policy
    </div>
  </div>
</div>
</body>
</html>


