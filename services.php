<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="description" content="Positive Project 2000 LTD - Main Building Contractors">
<meta name="keywords" content="main contractor, extension, conversion, conservation, renovation, brickwork, ground work, refurbishment, roof, electrical, plumbing, terrace, decoration, landscaping, prefabricated stairs, prefabricated concrete stairs, hampstead, london, camden, camden town">
<meta name="author" content="Mariusz Wasowski, Krzysztof Mazur">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" href="/favicon.gif" type="image/x-icon" />
<title>Services</title>
<script src="js/gallery.js"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54576502-1', 'auto');
  ga('send', 'pageview');

</script>
</head>

<body>
<div id="container">
	<div id="logo">
    <img src="img/positive-project_logo_large.jpg">
	</div>
    <?php 
	include ('./sections/announcement.php');
	?>
    <div id="menu">
    <?php 
	include ('./sections/menu.php');
	?>
	</div>
    <div id="gallery">
      
      <?php
	include'./slideshow/slideshow.php';
	?>
      
    </div>
    <div id="main">
   <div id="welcome">Services</div>
   <div id="main_text">
<p style="text-align:justify;">Positive Project 2000 employs over thirty full time operatives whose skills cover the full spectrum of building trades. We undertake projects that vary in value from less than £1,000 to over £1,000,000, ranging from minor repairs and redecoration to extensions, conversions, extensive renovations and new build developments.</p>
    <p>Each of our projects is afforded the same high level of professional and personal attention from our management team, staff and sub-contractors.</p>
<table style="width: 845px; height: 90px;" border="0">
<tbody>
<tr>
<td style="width: 167px;"><span style="color: black;"><span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">Extensions<br> Loft conversions<br> Garage conversions<br> Basements<br> Conservatories <br></span></span></span></td>
<td style="width: 167px;"><span style="color: black;"><span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">New Build<br> Renovation<br> Refurbishment</span><span style="font-size: small;"><br> Ground works&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br> Brickwork&nbsp; <br></span></span></span></td>
<td style="width: 167px;"><span style="color: black;"><span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">Roofing<br> Re-pointing<br> Lead work<br> Drainage<br> Carpentry&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br></span></span></span></td>
<td style="width: 167px;"><span style="color: black;"><span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">Joinery<br> Kitchens<br> Bathrooms<br> Plumbing &amp; Heating<br> Electrical services</span></span></span></td>
<td style="width: 167px;"><span style="color: black;"><span style="font-family: tahoma,arial,helvetica,sans-serif;"><span style="font-size: small;">Cornice<br>Decorating<br>Terraces<br>Driveways<br>Soft landscaping<br></span></span></span></td>
</tr>
</tbody>
</table>
	</div>
    </div>
    <div id="footer">
    <div id="footer1">
    Copyright © Positive Project 2000 Ltd 2014
    </div>
    <div id="footer2">
    Corporate Info | Terms & Conditions | Privacy Policy
    </div>
  </div>
</div>
</body>
</html>


